This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Background

This project was assigned to me as a means to gauge my technical ability. At its most basic, it required that I integrate
with an existing API, and be able to search and display information retrieved from the service.

## Considerations

Briefly, why I did what I did.

### Components

For this particular project, I decided to make use of existing Component Libraries, as well as develope my own to showcase
that I am capable of both approaches.

### State Management

I ended up making use of Redux Toolkit, which is an excellent tool for abstracting a lot of the boiler traditionally required
by redux. It also makes the code a little easier to reason about.

### Project Structure

Redux Toolkit required that state management logic utilise the 'ducks' pattern. All traditional aspects of redux
(actions, reducers, action creators) are now hosted in what is know as a slice. Major views are organised into features.
Features also allow for easy reuse among different projects, since we only need to import the feature folder and hook up
the already existing reducer.
Only truly reusable (ie can be reused by different features) components are added to the common folder.

### Code Quality

To keep my formatting and coding standards consistent, I opted to use prettier and eslint. I've also set up a pre-commit
hook to ensure that we do not commit poorly formatted code to the remote repo.

## Challenges

### Deezer API

It might be the case that I completely misunderstood the Deezer documentation, but I really struggled to get the
Javascript SDK up and running. As I work around I ended up proxying my request via https://cors-anywhere.herokuapp.com.
This has a huge disadvantage since I am only able to make 200 requests per hour. Thus I also had to debounce the search
quite a bit.

## Future Considerations

### Deezer Javascript SDK

Make use of the max range of requests available when using the SDK

### @media queries.

Although Grid does afford responsiveness out of the box, adding media queries and changing the layout to adapt
to different devices would be preferred.

### Testing

Adding a couple of test would ensure that we do not break our existing implementation as the app grows.

### Containerization (Docker)

Although I did not containerize the current implementation, doing so in future will eleviate the pain of dependency
management, deployment etc.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser (Should open up automatically).

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
