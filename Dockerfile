FROM node:alpine

WORKDIR '/app'

#add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

RUN echo 'Hello'
COPY package.json .
COPY package-lock.json .

RUN npm install

COPY . .

CMD ["npm", "start"]