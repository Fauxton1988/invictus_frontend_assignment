import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://cors-anywhere.herokuapp.com/http://api.deezer.com',
    headers: {
        'content-type': 'application/json',
    },
});

instance.interceptors.request.use(
    (response) => {
        return response;
    },
    (error) => {
        return Promise.reject(error);
    }
);

/* global DZ */
// if (DZ) {
//     let localDZ = DZ;
// }
export default {
    get: (url) =>
        instance({
            method: 'GET',
            url: `${url}`,
        }),
    // DZGet: (url) =>
    //     new Promise((resolve, reject) => {
    //         DZ.api(url, (error, response) => {
    //             if (error) {
    //                 reject(error);
    //             } else {
    //                 resolve(response);
    //             }
    //         });
    //     }),
};
