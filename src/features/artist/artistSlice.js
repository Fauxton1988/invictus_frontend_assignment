import { createSlice } from '@reduxjs/toolkit';

const artistInitialState = {
    activeArtist: {},
    status: 'idle',
    error: null,
};

const artistSlice = createSlice({
    name: 'artist',
    initialState: artistInitialState,
    reducers: {
        chooseArtist: (state, action) => {
            state.activeArtist = action.payload;
        },
    },
});

export const { chooseArtist } = artistSlice.actions;

export default artistSlice.reducer;
