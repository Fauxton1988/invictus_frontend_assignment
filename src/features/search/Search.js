import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useDebounce } from 'use-debounce';
import { performSearch } from './searchSlice';
import { chooseArtist } from '../artist/artistSlice';
import { ScaleLoader } from '../../common/ScaleLoader';

export const Search = () => {
    const dispatch = useDispatch();
    const searchResults = useSelector(({ search }) => search.searchResults);
    const artist = useSelector(({ artist }) => artist.activeArtist);
    const status = useSelector(({ search }) => search.status);

    const [displayResults, setDisplayResults] = useState(false);
    const [searchTerm, setSearchTerm] = useState('');
    const [debouncedTerm] = useDebounce(searchTerm, 300);

    useEffect(() => {
        if (debouncedTerm && artist.name !== searchTerm) {
            dispatch(performSearch(debouncedTerm));
        }
    }, [debouncedTerm, dispatch]);

    useEffect(() => {
        if (searchResults.length) {
            setDisplayResults(true);
        }
    }, [searchResults]);

    return (
        <div className="search-container">
            <input
                className="search-input"
                value={searchTerm}
                onChange={(e) => setSearchTerm(e.target.value)}
                type="text"
                placeholder="Search here..."
            />
            <ScaleLoader loading={status === 'loading'} />
            <button className="search-button">Search</button>
            <div
                className="search-results-text"
                style={{
                    display: artist.id && status !== 'loading' ? '' : 'none',
                }}
            >{`Search results for "${artist.name}"`}</div>

            <div
                className="search-results-list"
                style={{ display: displayResults ? '' : 'none' }}
            >
                <div className="arrow-up" />
                {searchResults.map((option) => (
                    <div
                        className="search-results-item"
                        onClick={() => {
                            setDisplayResults(false);
                            setSearchTerm(option.name);
                            dispatch(chooseArtist(option));
                        }}
                        key={option.id}
                    >
                        {option.name}
                    </div>
                ))}
            </div>
        </div>
    );
};
