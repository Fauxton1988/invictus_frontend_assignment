import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import api from '../../api';

const searchInitialState = {
    searchResults: [],
    status: 'idle',
    error: null,
};

export const performSearch = createAsyncThunk(
    'search/performSearch',
    async (term) => {
        const response = await api.get(`search/artist?q="${term}"`);

        return response.data.data;
    }
);

const searchSlice = createSlice({
    name: 'search',
    initialState: searchInitialState,
    reducers: {
        statusToIdle: (state) => {
            state.status = 'idle';
        },
    },
    extraReducers: {
        [performSearch.fulfilled]: (state, { payload }) => {
            state.status = 'succeeded';
            state.searchResults = payload;
        },
        [performSearch.pending]: (state) => {
            state.status = 'loading';
        },
        [performSearch.rejected]: (state, action) => {
            state.status = 'failed';
            state.error = action.error.message;
        },
    },
});

export const { statusToIdle } = searchSlice.actions;

export default searchSlice.reducer;
