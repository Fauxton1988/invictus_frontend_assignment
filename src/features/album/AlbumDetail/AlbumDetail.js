import React from 'react';
import { useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import './AlbumDetail.css';
import { ScaleLoader } from '../../../common/ScaleLoader';

const useStyles = makeStyles({
    container: {
        display: 'flex',
        justifyContent: 'flex-end',
        position: 'relative',
        // top: '-100px',
    },
    table: {
        width: '100%',
        color: 'white',
        justifySelf: 'center',
    },
    header: {
        color: '#8a8a8a',
        borderBottom: '1px solid #1c1c1c',
    },
    tableBody: {
        backgroundColor: '#292929',
    },
});

const formatSeconds = (seconds) => {
    const minutes = Math.floor(seconds / 60);
    const secs = seconds % 60 < 10 ? `0${seconds % 60}` : seconds % 60;
    return `${minutes}:${secs}`;
};

const extractYear = (release) => {
    return release.split('-')[0];
};

export const AlbumDetail = () => {
    const albumDetails = useSelector(({ album }) => album.albumDetail);
    const albumList = useSelector(({ album }) => album.albumList);
    const status = useSelector(({ album }) => album.status);

    const classes = useStyles();
    return albumDetails.id ? (
        <div className="album-detail-container">
            <div className="album-detail-img">
                <img
                    className="album-detail-img"
                    width="180"
                    src={albumDetails.cover_medium}
                />
            </div>
            <span className="album-detail-title">{albumDetails.title}</span>
            <div className="album-detail-space">
                <div className="album-detail-space-top" />
                <div className="album-detail-space-bottom" />
            </div>
            <div className="album-detail-table">
                <TableContainer className={classes.container}>
                    <Table
                        className={classes.table}
                        size="small"
                        aria-label="a dense table"
                    >
                        <TableHead>
                            <TableRow>
                                <TableCell className={classes.header}>
                                    #
                                </TableCell>
                                <TableCell
                                    className={classes.header}
                                    align="left"
                                >
                                    Title
                                </TableCell>
                                <TableCell
                                    className={classes.header}
                                    align="left"
                                >
                                    Artist
                                </TableCell>
                                <TableCell
                                    className={classes.header}
                                    align="left"
                                >
                                    Time
                                </TableCell>
                                <TableCell
                                    className={classes.header}
                                    align="left"
                                >
                                    Released
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody className={classes.tableBody}>
                            {albumDetails.tracks.data.map((row, index) => (
                                <TableRow key={row.name}>
                                    <TableCell
                                        className={classes.header}
                                        align="left"
                                    >
                                        {index + 1}
                                    </TableCell>
                                    <TableCell
                                        className={classes.header}
                                        align="left"
                                    >
                                        {row.title}
                                    </TableCell>
                                    <TableCell
                                        className={classes.header}
                                        align="left"
                                    >
                                        {albumDetails.artist.name}
                                    </TableCell>
                                    <TableCell
                                        className={classes.header}
                                        align="left"
                                    >
                                        {formatSeconds(row.duration)}
                                    </TableCell>
                                    <TableCell
                                        className={classes.header}
                                        align="left"
                                    >
                                        {extractYear(albumDetails.release_date)}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        </div>
    ) : (
        <div className="album-detail-loader">
            <ScaleLoader loading={status === 'loading' && albumList.length} />
        </div>
    );
};
