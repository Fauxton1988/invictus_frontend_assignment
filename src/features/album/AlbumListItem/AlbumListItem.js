import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { fetchAlbumDetails } from '../albumSlice';
import './AlbumListItem.css';

export const AlbumListItem = ({ id, cover_medium, title }) => {
    const dispatch = useDispatch();

    return (
        <>
            <div
                key={id}
                onClick={() => dispatch(fetchAlbumDetails(id))}
                className="album-item"
            >
                <img width="180" src={cover_medium} />
                <div className="truncated-title">{title}</div>
            </div>
        </>
    );
};

AlbumListItem.propTypes = {
    id: PropTypes.number.isRequired,
    cover_medium: PropTypes.string,
    title: PropTypes.string.isRequired,
};
