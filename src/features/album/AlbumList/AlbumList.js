import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BsChevronCompactLeft, BsChevronCompactRight } from 'react-icons/bs';
import { fetchArtistAlbums } from '../albumSlice';
import './AlbumList.css';
import { AlbumListItem } from '../AlbumListItem/AlbumListItem';
import { ScaleLoader } from '../../../common/ScaleLoader';

export const AlbumList = () => {
    const dispatch = useDispatch();
    const listRef = useRef();
    const artistId = useSelector(({ artist }) => artist.activeArtist.id);
    const albums = useSelector(({ album }) => album.albumList);
    const status = useSelector(({ album }) => album.status);

    useEffect(() => {
        if (artistId) {
            dispatch(fetchArtistAlbums(artistId));
        }
    }, [artistId]);

    const scrollLeft = () => {
        listRef.current.scrollLeft = listRef.current.scrollLeft - 210 * 4;
    };

    const scrollRight = () => {
        listRef.current.scrollLeft = listRef.current.scrollLeft + 210 * 4;
    };

    return (
        <>
            <div
                className="albums-heading"
                style={{ display: albums.length ? '' : 'none' }}
            >
                ALBUMS
                <div className="chevron-left" onClick={scrollLeft}>
                    <BsChevronCompactLeft />
                </div>
                <div className="chevron-right" onClick={scrollRight}>
                    <BsChevronCompactRight />
                </div>
            </div>
            <div className="album-list" ref={listRef}>
                {albums.length ? (
                    <>
                        {albums.map((album) => (
                            <AlbumListItem
                                key={album.id}
                                id={album.id}
                                title={album.title}
                                cover_medium={album.cover_medium}
                            />
                        ))}
                    </>
                ) : (
                    <div className="no-albums">
                        {status === 'loading' ? (
                            <ScaleLoader loading={true} />
                        ) : (
                            `No albums to display`
                        )}
                    </div>
                )}
            </div>
        </>
    );
};
