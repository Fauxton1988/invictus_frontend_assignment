import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import api from '../../api';

const albumInitialState = {
    albumList: [],
    albumDetail: {
        tracks: {
            data: [],
        },
    },
    status: 'idle',
    error: null,
};

export const fetchArtistAlbums = createAsyncThunk(
    'album/fetchArtistAlbums',
    async (id) => {
        const response = await api.get(`artist/${id}/albums`);

        return response.data.data;
    }
);

export const fetchAlbumDetails = createAsyncThunk(
    'album/fetchAlbumDetails',
    async (id) => {
        const response = await api.get(`album/${id}`);

        return response.data;
    }
);

const albumSlice = createSlice({
    name: 'album',
    initialState: albumInitialState,
    reducers: {},
    extraReducers: {
        [fetchArtistAlbums.fulfilled]: (state, { payload }) => {
            state.status = 'succeeded';
            state.albumList = payload;
        },
        [fetchArtistAlbums.pending]: (state) => {
            state.albumList = [];
            state.albumDetail = {
                tracks: {
                    data: [],
                },
            };
            state.status = 'loading';
        },
        [fetchArtistAlbums.rejected]: (state, action) => {
            state.status = 'failed';
            state.error = action.error.message;
        },
        [fetchAlbumDetails.fulfilled]: (state, { payload }) => {
            state.status = 'succeeded';
            state.albumDetail = payload;
        },
        [fetchAlbumDetails.pending]: (state) => {
            state.status = 'loading';
        },
        [fetchAlbumDetails.rejected]: (state, action) => {
            state.status = 'failed';
            state.error = action.error.message;
        },
    },
});

export default albumSlice.reducer;
