import { combineReducers } from '@reduxjs/toolkit';
import searchReducer from '../features/search/searchSlice';
import albumReducer from '../features/album/albumSlice';
import artistReducer from '../features/artist/artistSlice';

const rootReducer = combineReducers({
    search: searchReducer,
    album: albumReducer,
    artist: artistReducer,
});

export default rootReducer;
