import React from 'react';

import './App.css';
import '../features/search/Search.css';
import { Search } from '../features/search/Search';
import { AlbumList } from '../features/album/AlbumList/AlbumList';
import { AlbumDetail } from '../features/album/AlbumDetail/AlbumDetail';

function App() {
    return (
        <div className="App">
            <div className="auto-container">
                <Search />
                <AlbumList />
                <AlbumDetail />
            </div>
        </div>
    );
}

export default App;
