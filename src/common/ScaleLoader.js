import React from 'react';
import Loader from 'react-spinners/ScaleLoader';
import PropTypes from 'prop-types';

export const ScaleLoader = ({ loading }) => {
    return <Loader size={10} color={'#00efef'} loading={loading} />;
};

ScaleLoader.propTypes = {
    loading: PropTypes.bool,
};
